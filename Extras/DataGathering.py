import glob
import os
import sys
import random
import time
import numpy as np
import cv2
import weakref


try:
	sys.path.append(glob.glob('../carla/dist/carla-*%d.%d-%s.egg' % (
		sys.version_info.major,
		sys.version_info.minor,
		'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
	pass

import carla

IM_WIDTH = 640
IM_HEIGHT = 480

def process_img(image): #changing image to opencv format
	image.save_to_disk("C:/Users/ilank/Desktop/WindowsNoEditor/PythonAPI/examples/_out/im1.png") #change this path if you are not on the original pc
	i = np.array(image.raw_data)
	print(i.shape)
	i2 = i.reshape((IM_HEIGHT, IM_WIDTH))
	i3 = i2[:, :, :3]
	return i3/255.0


def set_segmentation(img): #function gets a picture from the sensor  and then saves it together with the current steering angle
	transform = vehicle.get_transform()
	spectator.set_transform(carla.Transform(transform.location + carla.Location(z=80),
	carla.Rotation(pitch=-90)))
	measurements, sensor_data = vehicle.read_data()
	control = measurements.player_measurements.autopilot_control
	vehicle.apply_control(carla.VehicleControl(throttle=0.3, steer=control.steer, brake=control.brake))
	img.convert(carla.ColorConverter.CityScapesPalette)
	img.save_to_disk('D:/DataSet/%08d/image' % img.frame_number) #change this path if you are not on the original pc
	print(control.steer)
	control = vehicle.get_control()
	f = open('D:/DataSet/%08d/data.txt' % img.frame_number, "a") #change this path if you are not on the original pc
	f.write("%f\n" % control.throttle)
	f.write("%f\n" % control.steer)
	f.write("%f" % control.brake)
	f.close()


def read_data():
		"""
		Read the data sent from the server this frame. The episode must be
		started. Return a pair containing the protobuf object containing the
		measurements followed by the raw data of the sensors.
		"""
		data = client.read()
		if not data:
			raise RuntimeError('failed to read data from server')
		pb_message = carla_protocol.Measurements()
		pb_message.ParseFromString(data)

		return pb_message, dict(x for x in self._read_sensor_data())


actor_list = []
try:
	client = carla.Client("127.0.0.1", 2000)
	client.set_timeout(10.0)

	world = client.load_world('Town04')
	blueprint_library = world.get_blueprint_library()

	bp = blueprint_library.filter("model3")[0]
	print(bp)

	spawn_point = random.choice(world.get_map().get_spawn_points())

	vehicle = world.spawn_actor(bp, spawn_point)
	vehicle.set_autopilot(True)
	actor_list.append(vehicle)

	cam_bp = blueprint_library.find("sensor.camera.semantic_segmentation")
	cam_bp.set_attribute("image_size_x", f"{IM_WIDTH}")
	cam_bp.set_attribute("image_size_y", f"{IM_HEIGHT}")
	cam_bp.set_attribute("fov", "120")

	spawn_point = carla.Transform(carla.Location(x=3, z=0.7))
	sensor = world.spawn_actor(cam_bp, spawn_point, attach_to=vehicle)
	actor_list.append(sensor)

	spectator = world.get_spectator()
	transform = vehicle.get_transform()
	actor_list.append(spectator)

	sensor.listen(lambda image: set_segmentation(image))

	time.sleep(900)
finally:
	for actor in actor_list:
		actor.destroy()
	print("Finished cleaning!")

if __name__ == '__main__':

	main()
