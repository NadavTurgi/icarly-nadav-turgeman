import glob
import sys
import random
import time
import numpy as np
import cv2
import weakref
from shutil import copyfile

import os
import pandas as pd
import seaborn as sns
import pprint
import matplotlib.pyplot as plt
import shutil
count = 0
basepath = 'D:/DataSet' #change this path if you are not on the original pc


DataFromFiles=[]
for file in os.listdir(basepath):
    pprint.pprint(f"Processing file {file}")
    # Using readlines()
    fileHandle = open(os.path.join(basepath + "/" + file + "/data.txt"), 'r')
    Lines = fileHandle.readlines()

    count = 0
    values_dict={}
    values_dict['Power']=float(Lines[0])
    values_dict['Angle'] = float(Lines[1])
    values_dict['Breaks'] = float(Lines[2])
    DataFromFiles.append(values_dict)

DataFrameForAnalysis=pd.DataFrame(DataFromFiles)
pprint.pprint(DataFrameForAnalysis.head(10))
fig, axes = plt.subplots(1, 3, figsize=(15, 5), sharey=True)
sns.despine(left=True)
fig.suptitle('Carla data set')

sns.displot(ax=axes[0], data=DataFrameForAnalysis, x='Power')
axes[0].set_title('Power')
sns.displot(ax=axes[1], data=DataFrameForAnalysis, x='Angle')
axes[1].set_title('Angle')
sns.displot(ax=axes[2], data=DataFrameForAnalysis, x='Breaks')
axes[2].set_title('Breaks')

plt.tight_layout()
plt.show()