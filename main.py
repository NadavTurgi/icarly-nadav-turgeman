import glob
import os
import sys
import random
import time
import numpy as np
import cv2
import weakref
import logging
import math
import speech_recognition as sr

# tensorflow
import tensorflow as tf
import keras
from tensorflow.keras.models import load_model


recognizer = sr.Recognizer()
mic = sr.Microphone()


try:
	sys.path.append(glob.glob('../carla/dist/carla-*%d.%d-%s.egg' % (
		sys.version_info.major,
		sys.version_info.minor,
		'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
	pass


import carla


IM_WIDTH2 = 800
IM_HEIGHT2 = 450

IM_WIDTH = 200
IM_HEIGHT = 66

STOP = False
SPEED = 0.3


class SignClassifier(object):
    def __init__(self):
        PATH_TO_MODEL = 'models/frozen_inference_graph.pb'
        self.detection_graph = tf.Graph()
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(PATH_TO_MODEL, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')
            self.image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
            self.d_boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
            self.d_scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
            self.d_classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
            self.num_d = self.detection_graph.get_tensor_by_name('num_detections:0')
        self.sess = tf.Session(graph=self.detection_graph)

    def get_classification(self, img):
        """
        Detects bounding boxes for objects in an image.
        Gets an image as an input. Returns the bounding boxes, the score for each box,
        the classification for each box and the number of boxes.
        """
        with self.detection_graph.as_default():
            img_expanded = np.expand_dims(img, axis=0)
            (boxes, scores, classes, num) = self.sess.run(
                [self.d_boxes, self.d_scores, self.d_classes, self.num_d],
                feed_dict={self.image_tensor: img_expanded})
        return boxes, scores, classes, num


def img_preprocess(image):
        """
        changes the format of an image so it fits the lane model.
        Gets an image as input and outputs the new image.
        """
    image = cv2.cvtColor(image, cv2.COLOR_RGB2YUV)  # Nvidia model said it is best to use YUV color space
    image = cv2.GaussianBlur(image, (3,3), 0) # input image size (200,66) Nvidia model
    image = tf.cast(image, tf.float32) / 255
    return image


def set_rgb(img): 
        """
        logic funtion for the sign recognition model
        Gets an image as an input. Doesnt return anything.
        """
    global STOP
    global SPEED
    img.convert(carla.ColorConverter.CityScapesPalette)
    imageArr = (np.array(img.raw_data).reshape((IM_HEIGHT, IM_WIDTH, 4))
    image = imageArr[:, :, :3]
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    oxes, scores, classes, num = SignDetector.get_classification(image)
    if 'stop' in classes:
        STOP = True
    elif 'slow' in classes:
        SPEED = 0.3
    elif 'fast' in classes:
        SPEED = 0.7



def set_segmentation(img):
        """
        main logic of the car. uses the lane detection model until there is a stop sign
        Gets an image as an input. Doesnt return anything.
        """
    global STOP
    global SPEED
    transform = vehicle.get_transform()
    spectator.set_transform(carla.Transform(transform.location + carla.Location(z=30),
    carla.Rotation(pitch=-90)))
    img.convert(carla.ColorConverter.CityScapesPalette)
    imageArr = (np.array(img.raw_data).reshape((IM_HEIGHT, IM_WIDTH, 4))
    image = imageArr[:, :, :3]
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    img_list = [img_preprocess(image)]
    if STOP:
        vehicle.apply_control(carla.VehicleControl(throttle=0, brake=1))
        flag = True
        while flag:
            print("listening now")
            with mic as source:
                recognizer.adjust_for_ambient_noise(source)
                audio = recognizer.listen(source)
            x = recognizer.recognize_google(audio, language="he-HE")
            if x == 'ימינה':
                print("turning right")
                flag = False
                timeout = time.time() + 7.5   # 8 seconds from now
                while True:
                    if time.time() > timeout:
                        break
                    vehicle.apply_control(carla.VehicleControl(throttle=SPEED, steer=0.28))
            elif x == 'שמאלה':
                print("turning left")
                flag = False
                timeout = time.time() + 9   # 8 seconds from now
                while True:
                    if time.time() > timeout:
                        break
                    vehicle.apply_control(carla.VehicleControl(throttle=SPEED, steer=-0.23))
            elif x == 'ישר':
                print("going straight")
                flag = False  
                timeout = time.time() + 10   # 8 seconds from now
                while True:
                    if time.time() > timeout:
                        break
                    vehicle.apply_control(carla.VehicleControl(throttle=SPEED))
            STOP = False
    else:
        steer_ang = (((float(model.predict(np.asarray(img_list))) * 0.833333) + 15.0) / 90.0) - 1.0
        vehicle.apply_control(carla.VehicleControl(throttle=SPEED, steer=steer_ang))


def read_data():
        """
        Read the data sent from the server this frame. The episode must be
        started. Return a pair containing the protobuf object containing the
        measurements followed by the raw data of the sensors.
        """
        data = client.read()
        if not data:
            raise RuntimeError('failed to read data from server')
        pb_message = carla_protocol.Measurements()
        pb_message.ParseFromString(data)

        return pb_message, dict(x for x in self._read_sensor_data())


actor_list = []
model = load_model('models/lane_navigation_eluFinal.h5')

#Connection to simulation and creation of actors in the map.
try:
    client = carla.Client("127.0.0.1", 2000)
    client.set_timeout(20.0)

    for actor in actor_list:
        actor.destroy()

    world = client.load_world('Town04')
    blueprint_library = world.get_blueprint_library()

    bp = blueprint_library.filter("model3")[0]
    print(bp)
    SignDetector = SignClassifier()
    spawn_point = world.get_map().get_spawn_points()[10]

    vehicle = world.spawn_actor(bp, spawn_point)
    actor_list.append(vehicle)

    cam_bp = blueprint_library.find("sensor.camera.semantic_segmentation")
    cam_bp.set_attribute("image_size_x", f"{IM_WIDTH}")
    cam_bp.set_attribute("image_size_y", f"{IM_HEIGHT}")
    cam_bp.set_attribute("fov", "120")

    spawn_point = carla.Transform(carla.Location(x=3, y=-0.5, z=0.7))
    sensor = world.spawn_actor(cam_bp, spawn_point, attach_to=vehicle)
    actor_list.append(sensor)

    cam_bp2 = blueprint_library.find("sensor.camera.rgb")
    cam_bp2.set_attribute("image_size_x", f"{IM_WIDTH2}")
    cam_bp2.set_attribute("image_size_y", f"{IM_HEIGHT2}")
    cam_bp2.set_attribute("fov", "120")

    spawn_point2 = carla.Transform(carla.Location(x=3, y=-0.5, z=0.7))
    sensor2 = world.spawn_actor(cam_bp2, spawn_point2, attach_to=vehicle)
    actor_list.append(sensor2)

    spectator = world.get_spectator()
    transform = vehicle.get_transform()
    actor_list.append(spectator)

    sensor.listen(lambda image: set_segmentation(image))
    sensor2.listen(lambda image: set_rgb(image))
    time.sleep(200)
finally:
    for actor in actor_list:
        actor.destroy()
    print("Finished cleaning!")

