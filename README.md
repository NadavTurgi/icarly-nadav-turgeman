# iCarly
## _intelligent car loves you_

iCarly is an autonomous car that works using machine learning in the Carla environment which is based on UnrealEngine

## Features

- The car can drive in a road beween the lane lines
- the car can recognize stop signs and speed signs and acts accordingly
- when the car stops at a crossroad it waits for a voice command in Hebrew that will tell it where to go next 




## Installation

iCarly requires python 3.7.9 in order to run

Install the next modules in order to be able to run the project

```sh
pip install tensorflow
pip install tensorflow-gpu
pip install numpy
pip install scikit-learn
pip install keras
pip install pandas
pip install opencv-python
pip install keras-vis
pip install scipy
pip install torch
pip install torchvision
```

In addition you will need to download the NVIDIA GPU Computing Toolkit according to the version that is written when you run our code.

You can find all version of the Toolkits and cudnn in the following links
| file | link |
| ------ | ------ |
| Toolkit | https://developer.nvidia.com/cuda-toolkit-archive |
| cudnn | https://developer.nvidia.com/rdp/cudnn-archive |


## Carla
next you will need to build Carla according to the steps in the next link: https://carla.readthedocs.io/en/latest/build_windows/


## Activating the car

After following the windows build guide for carla, activate the unreal editor according to the last step in that guide and press play in order to start the simulation.
Then in the files of the unreal project find map4 and then double click it in order to change to that map.
Next you will need to run take our repository and place it inside carla\PythonAPI\examples.
After that, run main.py and a car should spawn in the simulation.

## controling the car

The car drives mostly independently however when it reaches a crossroad, it will stop and wait for a voice command.
when the car stops, wait for a second or half a second and then say clearly in Hebrew which way you want to go ("שמאלה"/"ימינה"/"ישר").


## Other files

In our repository you can also find the file DataGathering.py which is used to create the data set for the model responsible to driving between the lane lines.
In order to create the sign data set for the sign detection model you need to place signs in a map of the unreal project in different positions, take pictures of them and then lable those pictures.
We used a very handy tool call labelimg which can be used in order to mark rectangles on each picture and define what type of sign is there and then it automaticaly creates xml files for each picture, describing where each sign is positioned in the picture.

In order to train our models we used the next colab guides:

| model | link |
| ------ | ------ |
| lane nevigation | https://colab.research.google.com/drive/1U29NyT8QL8KpGYgSypg5QUOlzmFVqBR5?usp=sharing |
| sign detection | https://colab.research.google.com/drive/1mrRI2FDS3GAWVNHU_s7TN0FBM9dyZtm6?usp=sharing |




